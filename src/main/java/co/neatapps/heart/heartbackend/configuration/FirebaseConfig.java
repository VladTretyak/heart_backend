package co.neatapps.heart.heartbackend.configuration;

import co.neatapps.heart.heartbackend.utils.Constants;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

@Configuration
public class FirebaseConfig {
    @Bean
    FirebaseApp createFireBaseApp() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(Constants.SECRET_FILE);
        if (resource == null) {
            throw new NullPointerException("There is not the Firebase admin secret file!");
        }
        String file = resource.getFile();
        FileInputStream serviceAccount = new FileInputStream(file);

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl(Constants.DATABASE_URL)
                .build();

        return  FirebaseApp.initializeApp(options);
    }

}