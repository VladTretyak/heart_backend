package co.neatapps.heart.heartbackend.configuration;


import co.neatapps.heart.heartbackend.domain.User;
import co.neatapps.heart.heartbackend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer.AuthorizedUrl;

@Configuration
@EnableWebSecurity
@EnableOAuth2Sso
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private RESTAuthenticationEntryPoint authenticationEntryPoint;

    public WebSecurityConfig() {
    }

    protected void configure(HttpSecurity http) throws Exception {
        AuthorizedUrl authorizedUrl = http.authorizeRequests()
                .mvcMatchers(new String[]{"/", "/storm/send", "/login", "/logout", "/user/authByToken"})
                .permitAll()
                .mvcMatchers(HttpMethod.OPTIONS, new String[]{"/*", "/storm/*", "/user/*"}).permitAll()
                .anyRequest();


        HttpSecurity httpSecurity = (HttpSecurity) authorizedUrl
                .authenticated()
                .and();

        httpSecurity.csrf().disable()
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
    }

    @Bean
    public PrincipalExtractor principalExtractor(UserRepository userRepository) {
        return (map) -> {
            String id = (String) map.get("sub");
            User user = userRepository.findById(id).orElseGet(() -> {
                User newUser = new User();
                newUser.setUserId(id);
                newUser.setName((String) map.get("name"));
                newUser.setEmail((String) map.get("email"));
                newUser.setGivenName((String) map.get("given_name"));
                newUser.setFamilyName((String) map.get("family_name"));
                newUser.setPhoto((String) map.get("picture"));
                return newUser;
            });
            return userRepository.save(user);
        };
    }
}
