package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.Comment;

public interface CommentServices {
    Comment save(Comment comment);

    Comment findById(Long id);

    void delete(Long id);


}
