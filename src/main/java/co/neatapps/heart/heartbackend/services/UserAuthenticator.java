package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.User;

public interface UserAuthenticator {
    User authenticateUser(String tokenId);
}
