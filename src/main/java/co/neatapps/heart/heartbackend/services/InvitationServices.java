package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.Invitation;
import co.neatapps.heart.heartbackend.domain.User;

import java.util.List;

public interface InvitationServices {

    void save(Invitation invitation);

    List<Invitation> findMyInvitations(User owner, String email);

    Invitation findById(Long id);

    boolean checkInvitationPermission(Invitation invitation);

    void shareAccordingRole(Invitation invitation);

    void deleteInvitationById(Long id);
}
