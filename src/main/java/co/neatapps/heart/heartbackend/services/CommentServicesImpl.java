package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.Comment;
import co.neatapps.heart.heartbackend.repositories.CommentRepository;
import org.springframework.stereotype.Service;

@Service
public class CommentServicesImpl implements CommentServices {

    private final CommentRepository commentRepository;

    public CommentServicesImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public Comment save(Comment comment) {
        return this.commentRepository.save(comment);
    }

    @Override
    public Comment findById(Long id) {
        return this.commentRepository.getOne(id);
    }

    @Override
    public void delete(Long id) {
        this.commentRepository.deleteById(id);
    }
}
