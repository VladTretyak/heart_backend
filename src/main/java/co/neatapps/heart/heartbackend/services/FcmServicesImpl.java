package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.RemoteNotification;
import com.google.firebase.messaging.*;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class FcmServicesImpl implements FcmServices {

    private static final Long MAX_TIME_TO_LIVE = 1728000L;

    public TopicManagementResponse addTokenToTopic(String userId, String token) throws FirebaseMessagingException {
        return FirebaseMessaging.getInstance().subscribeToTopic(
                Collections.singletonList(token), userId);
    }

    public void sendPushNotification(String userId, RemoteNotification notification) throws FirebaseMessagingException {
        Message message = Message.builder()
                .putAllData(notification.getAllData())
                .setTopic(userId)
                .build();
        FirebaseMessaging.getInstance().send(message);
    }

    /**
     * We can use this method for customize additional notification settings.
     *
     * @param channelId
     * @return androidConfig
     * @see AndroidConfig
     */
    private AndroidConfig getAndroidConfig(String channelId) {
        return AndroidConfig.builder()
                .setTtl(MAX_TIME_TO_LIVE)
                .setNotification(AndroidNotification.builder()
                        .setChannelId(channelId)
                        .build())
                .build();
    }

    /**
     * We can use this method for customize additional notification settings.
     *
     * @param channelId
     * @return apnsConfig
     * @see ApnsConfig
     */
    private ApnsConfig getApnsConfig(String channelId) {
        return ApnsConfig.builder()
                .putHeader("apns-expiration", Long.toString(System.currentTimeMillis() / 1000 + MAX_TIME_TO_LIVE))
                .setAps(Aps.builder()
                        .setThreadId(channelId)
                        .build())
                .build();
    }

    /**
     * We can use this method for customize additional notification settings.
     *
     * @return webPushConfig
     * @see WebpushConfig
     */
    private WebpushConfig getWepConfig() {
        return WebpushConfig.builder()
                .build();
    }
}