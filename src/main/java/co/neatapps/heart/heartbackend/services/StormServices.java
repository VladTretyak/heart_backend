package co.neatapps.heart.heartbackend.services;


import co.neatapps.heart.heartbackend.domain.Storm;
import co.neatapps.heart.heartbackend.domain.User;

import java.util.List;


public interface StormServices {
    Storm findById(Integer var1);

    List<Storm> findByUserId(String var1);

    Integer saveStorm(Storm var1);

    void deleteStormById(Integer var1);

    boolean checkStormPermission(User var1);

    boolean checkStormPermission(Storm var1);
}
