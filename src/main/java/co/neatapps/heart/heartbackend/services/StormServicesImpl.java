package co.neatapps.heart.heartbackend.services;


import co.neatapps.heart.heartbackend.domain.Storm;
import co.neatapps.heart.heartbackend.domain.User;
import co.neatapps.heart.heartbackend.repositories.StormRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class StormServicesImpl implements StormServices {
    private final StormRepository stormRepository;
    private final UserServices userServices;

    public StormServicesImpl(StormRepository stormRepository, UserServices userServices) {
        this.stormRepository = stormRepository;
        this.userServices = userServices;
    }

    public Storm findById(Integer id) {
        return this.stormRepository.findByStormId(id);
    }

    private List<Storm> sortHeartQuestions(List<Storm> storms) {
        for (int i = 0; i < storms.size(); i++) {
            Collections.sort(storms.get(i).getHeartQuestions(),
                    Comparator.comparingInt(o -> o.getHeartQuestionType().ordinal())
            );
        }
        return storms;
    }

    public List<Storm> findByUserId(String userId) {
        List<Storm> storms = this.stormRepository.findByUserId(userId);
        return sortHeartQuestions(storms);
    }

    public Integer saveStorm(Storm storm) {
        return (Integer) (this.stormRepository.save(storm)).getStormId();
    }

    public void deleteStormById(Integer id) {
        stormRepository.deleteById(id);
    }

    public boolean checkStormPermission(User user) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getSharedTo().contains(currentUser);
    }

    public boolean checkStormPermission(Storm storm) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User stormOwner = userServices.findUserById(storm.getUserId());

        return (stormOwner.equals(currentUser) || stormOwner.getSharedTo().contains(currentUser));
    }

}
