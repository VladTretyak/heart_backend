package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.HeartQuestion;
import co.neatapps.heart.heartbackend.repositories.HeartQuestionRepository;
import org.springframework.stereotype.Service;

@Service
public class HeartQuestionServicesImpl implements HeartQuestionServices {

    private final HeartQuestionRepository heartQuestionRepository;

    public HeartQuestionServicesImpl(HeartQuestionRepository heartQuestionRepository) {
        this.heartQuestionRepository = heartQuestionRepository;
    }


    public void saveHeartQuestion(HeartQuestion heartQuestion) {
        heartQuestionRepository.save(heartQuestion);
    }
}
