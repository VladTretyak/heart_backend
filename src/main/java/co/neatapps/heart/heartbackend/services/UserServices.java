package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.User;

import java.util.List;

public interface UserServices {
    User findUserById(String var1);

    User findUserByFamilyName(String var1);

    void save(User var1);

    List<User> findAll();

    User findByEmail(String email);

    List<User> getSharedWithMe(User var1);

    List<User> getMySharedTo(String var1);
}
