package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.HeartQuestion;

public interface HeartQuestionServices {
    void saveHeartQuestion(HeartQuestion heartQuestion);
}
