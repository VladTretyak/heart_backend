package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.User;
import co.neatapps.heart.heartbackend.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServicesImpl implements UserServices {
    private final UserRepository userRepository;

    public UserServicesImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findUserById(String id) {
        return this.userRepository.findByUserId(id);
    }

    public User findUserByFamilyName(String familyName) {
        return this.userRepository.findByFamilyName(familyName);
    }

    private User updateUser(User oldUser, User newUser) {
        newUser.setSharedTo(oldUser.getSharedTo());
        return newUser;
    }

    public void save(User user) {
        User existingUser = findUserById(user.getUserId());
        if (existingUser != null) {
            this.userRepository.save(updateUser(existingUser, user));
        } else {
            this.userRepository.save(user);
        }
    }

    public List<User> getMySharedTo(String id) {
        return this.userRepository.findByUserId(id).getSharedTo();
    }

    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public List<User> getSharedWithMe(User user) {
        return this.userRepository.findBySharedToContains(user);
    }
}
