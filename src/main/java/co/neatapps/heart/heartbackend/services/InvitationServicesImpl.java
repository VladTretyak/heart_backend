package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.Invitation;
import co.neatapps.heart.heartbackend.domain.User;
import co.neatapps.heart.heartbackend.repositories.InvitationRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvitationServicesImpl implements InvitationServices {

    public final InvitationRepository invitationRepository;
    public final UserServices userServices;

    public InvitationServicesImpl(InvitationRepository invitationRepository, UserServices userServices) {
        this.invitationRepository = invitationRepository;
        this.userServices = userServices;
    }

    public void deleteInvitationById(Long id) {
        invitationRepository.deleteById(id);
    }

    public void save(Invitation invitation) {
        this.invitationRepository.save(invitation);
    }

    public List<Invitation> findMyInvitations(User owner, String email) {
        return invitationRepository.findByOwnerOrEmailOfInvited(owner, email);
    }

    public Invitation findById(Long id) {
        return this.invitationRepository.getOne(id);
    }

    public boolean checkInvitationPermission(Invitation invitation) {

        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return currentUser.equals(invitation.getOwner()) || currentUser.getEmail().equals(invitation.getEmailOfInvited());

    }

    public void shareAccordingRole(Invitation invitation) {
        switch (invitation.getRoleOfInvited()) {
            case COUNSELOR:
                saveCounselor(invitation);
                break;

            case WARD:
                saveWard(invitation);
                break;
        }
    }

    private void saveCounselor(Invitation invitation) {
        User owner = invitation.getOwner();
        owner.getSharedTo().add(userServices.findByEmail(invitation.getEmailOfInvited()));
        userServices.save(owner);
    }

    private void saveWard(Invitation invitation) {
        User ward = userServices.findByEmail(invitation.getEmailOfInvited());
        ward.getSharedTo().add(invitation.getOwner());
        userServices.save(ward);
    }
}


