package co.neatapps.heart.heartbackend.services;

import co.neatapps.heart.heartbackend.domain.RemoteNotification;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.TopicManagementResponse;

public interface FcmServices {
    void sendPushNotification(String userId, RemoteNotification notification) throws FirebaseMessagingException;

    TopicManagementResponse addTokenToTopic(String userId, String token) throws FirebaseMessagingException;
}
