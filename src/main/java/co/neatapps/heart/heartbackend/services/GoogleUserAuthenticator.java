package co.neatapps.heart.heartbackend.services;


import co.neatapps.heart.heartbackend.domain.User;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

@Service
public class GoogleUserAuthenticator implements UserAuthenticator {
    private static final String CLIENT_ID_WEB = "774554471186-s24m6fn4et9f2cqr9n7d8s4sb97lhvtk.apps.googleusercontent.com";
    private static final String CLIENT_ID_IOS = "774554471186-52ifk6h29p97v93bl9u01621c37cpf4q.apps.googleusercontent.com";
    private static final String CLIENT_ID_ANDROID = "774554471186-pgisss8lustnghcsp5a3q8vv0bs7tb5c.apps.googleusercontent.com";
    private static final Logger LOG = LoggerFactory.getLogger(GoogleUserAuthenticator.class);

    @Override
    public User authenticateUser(String tokenId) {
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        HttpTransport httpTransport = createHttpTransport();
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(httpTransport, jsonFactory)
                .setAudience(Arrays.asList(CLIENT_ID_WEB, CLIENT_ID_IOS, CLIENT_ID_ANDROID))
                .build();

        GoogleIdToken idToken = getGoogleIdToken(tokenId, verifier);
        User newUser = extractUserData(idToken);

        return newUser;
    }

    private User extractUserData(GoogleIdToken idToken) {
        GoogleIdToken.Payload payload = idToken.getPayload();

        String userId = payload.getSubject();
        String email = payload.getEmail();
        String name = (String) payload.get("name");
        String pictureUrl = (String) payload.get("picture");
        String familyName = (String) payload.get("family_name");
        String givenName = (String) payload.get("given_name");

        return new User(userId, name, pictureUrl, email, familyName, givenName);
    }

    private GoogleIdToken getGoogleIdToken(String tokenId, GoogleIdTokenVerifier verifier) {
        GoogleIdToken idToken = null;
        try {
            idToken = verifier.verify(tokenId);
        } catch (GeneralSecurityException | IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return idToken;
    }

    private HttpTransport createHttpTransport() {
        HttpTransport httpTransport = null;
        try {
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        } catch (GeneralSecurityException | IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return httpTransport;
    }

}