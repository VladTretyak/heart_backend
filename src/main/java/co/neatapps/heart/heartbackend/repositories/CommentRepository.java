package co.neatapps.heart.heartbackend.repositories;

import co.neatapps.heart.heartbackend.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {

}
