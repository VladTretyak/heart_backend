package co.neatapps.heart.heartbackend.repositories;

import co.neatapps.heart.heartbackend.domain.Storm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StormRepository extends JpaRepository<Storm, Integer> {
    List<Storm> findByUserId(String var1);

    Storm findByStormId(Integer id);

}