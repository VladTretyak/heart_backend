package co.neatapps.heart.heartbackend.repositories;

import co.neatapps.heart.heartbackend.domain.HeartQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HeartQuestionRepository extends JpaRepository<HeartQuestion, Integer> {

}
