package co.neatapps.heart.heartbackend.repositories;


import co.neatapps.heart.heartbackend.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, String> {
    User findByFamilyName(String var1);

    User findByEmail(String var1);

    User findByUserId(String var1);

    List<User> findBySharedToContains(User var1);
}
