package co.neatapps.heart.heartbackend.repositories;

import co.neatapps.heart.heartbackend.domain.Invitation;
import co.neatapps.heart.heartbackend.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvitationRepository extends JpaRepository<Invitation, Long> {
    List<Invitation> findByOwnerOrEmailOfInvited(User owner, String email);
}
