package co.neatapps.heart.heartbackend.controllers;

import co.neatapps.heart.heartbackend.domain.RemoteNotification;
import co.neatapps.heart.heartbackend.domain.Storm;
import co.neatapps.heart.heartbackend.domain.User;
import co.neatapps.heart.heartbackend.services.FcmServices;
import co.neatapps.heart.heartbackend.services.StormServices;
import co.neatapps.heart.heartbackend.services.UserServices;
import co.neatapps.heart.heartbackend.utils.Constants;
import com.google.firebase.messaging.FirebaseMessagingException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping({"storm"})
public class StormController {

    private final StormServices stormServices;
    private final UserServices userServices;
    private final FcmServices fcmServices;

    public StormController(StormServices stormServices, UserServices userServices, FcmServices fcmServices) {
        this.stormServices = stormServices;
        this.userServices = userServices;
        this.fcmServices = fcmServices;
    }

    private void notifyCounselors(User currentUser, int stormId) throws FirebaseMessagingException {
        List<User> counselors = userServices.getMySharedTo(currentUser.getUserId());

        RemoteNotification notification = RemoteNotification.builder()
                .setTitle(currentUser.getName())
                .setBody("Добавил новый шторм")
                .setChannelId(Constants.STORM_CHANNEL)
                .putData("stormId", String.valueOf(stormId))
                .build();

        for (User counselor : counselors) {
            fcmServices.sendPushNotification(counselor.getUserId(), notification);
        }
    }

    @PostMapping
    public Integer saveStorm(@RequestBody Storm storm) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        storm.setUserId(currentUser.getUserId());
        Integer savedStormId = stormServices.saveStorm(storm);
        new Thread(() -> {
            try {
                notifyCounselors(currentUser, savedStormId);
            } catch (FirebaseMessagingException e) {
                e.printStackTrace();
            }
        }).start();
        return savedStormId;
    }

    @GetMapping
    public List<Storm> getStormsOfCurrentUser() {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return stormServices.findByUserId(currentUser.getUserId());
    }

    @GetMapping("{id}")
    public Storm findStormById(@PathVariable Integer id, HttpServletResponse response) {
        Storm storm = stormServices.findById(id);
        if (stormServices.checkStormPermission(storm)) {
            response.setStatus(HttpServletResponse.SC_OK);
            return storm;
        }
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        return null;
    }

    @DeleteMapping("{id}")
    public void deleteStorm(@PathVariable Integer id) {
        Storm storm = stormServices.findById(id);
        if (stormServices.checkStormPermission(storm)) {
            stormServices.deleteStormById(id);
        }
    }

    @GetMapping("/user/{user}")
    public List<Storm> getStormsOfUser(@PathVariable User user, HttpServletResponse response) {
        if (!stormServices.checkStormPermission(user)) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
        return stormServices.findByUserId(user.getUserId());
    }
}
