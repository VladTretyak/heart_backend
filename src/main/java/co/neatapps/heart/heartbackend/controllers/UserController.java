package co.neatapps.heart.heartbackend.controllers;


import co.neatapps.heart.heartbackend.domain.*;
import co.neatapps.heart.heartbackend.services.*;
import co.neatapps.heart.heartbackend.utils.Constants;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.TopicManagementResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping({"user"})
public class UserController {
    private final UserServices userServices;
    private final InvitationServices invitationServices;
    private final List<GrantedAuthority> authorities = Collections.singletonList(new GrantedAuthorityImpl(AuthorityRoles.USER));
    private final FcmServices fcmServices;


    public UserController(UserServices userServices, InvitationServices invitationServices, FcmServices fcmServices) {
        this.userServices = userServices;
        this.invitationServices = invitationServices;
        this.fcmServices = fcmServices;
    }

    @GetMapping
    public List<User> getAllUsers() {
        return this.userServices.findAll();
    }

    @GetMapping({"mySharedTo"})
    public List<User> getMySharedTo() {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return this.userServices.getMySharedTo(currentUser.getUserId());
    }

    @GetMapping({"me"})
    public @ResponseBody
    User getCurrentUserInfo() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @PostMapping({"invite"})
    public void inviteUser(@RequestBody Invitation invitation) {
        invitation.setOwner((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        invitation.setStatus(InvitationStatus.PENDING);
        invitation.setInvitationDate(new Date());
        User invitedUser = userServices.findByEmail(invitation.getEmailOfInvited());
        if (invitedUser != null) {
            invitation.setNameOfInvited(invitedUser.getName());
            invitation.setUserIdOfInvited(invitedUser.getUserId());
            invitation.setPhotoOfInvited(invitedUser.getPhoto());
        }
        invitationServices.save(invitation);  // todo: figure out what to do with client generated invitation.id
        if (invitedUser == null) {
            //TODO: Send invitation e-mail
        } else {
            new Thread(() -> {
                try {
                    notifyNewInvitation(invitation);
                } catch (FirebaseMessagingException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

    @DeleteMapping("invitation/{id}")
    public void deleteInvitation(@PathVariable Long id) {
        Invitation invitation = invitationServices.findById(id);
        if (invitationServices.checkInvitationPermission(invitation)) {
            invitationServices.deleteInvitationById(id);
        }
    }

    private void notifyNewInvitation(Invitation invitation) throws FirebaseMessagingException {
        RemoteNotification notification = RemoteNotification.builder()
                .setTitle("Новое приглашение")
                .setBody("У вас тут новое приглашение от " + invitation.getOwner().getName())
                .setChannelId(Constants.INVITATION_CHANNEL)
                .putData("invited_as", invitation.getRoleOfInvited().toString())
                .build();

        User invited = userServices.findByEmail(invitation.getEmailOfInvited());
        fcmServices.sendPushNotification(invited.getUserId(), notification);
    }

    @GetMapping({"myInvitations"})
    public List<Invitation> getMyInvitations() {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return invitationServices.findMyInvitations(currentUser, currentUser.getEmail());
    }

    @GetMapping({"acceptInvitation/{id}"})
    public ResponseEntity<String> acceptInvitation(@PathVariable Long id) {
        Invitation invitation = invitationServices.findById(id);

        if (invitationServices.checkInvitationPermission(invitation) && invitation.getStatus() != InvitationStatus.ACCEPTED) {
            invitation.setStatus(InvitationStatus.ACCEPTED);
            invitationServices.save(invitation);
            invitationServices.shareAccordingRole(invitation);

            return returnOK();
        }

        return rejectInvitationHandle();
    }

    @GetMapping("declineInvitation/{id}")
    public ResponseEntity<String> declineInvitation(@PathVariable Long id) {

        Invitation invitation = invitationServices.findById(id);

        if (invitationServices.checkInvitationPermission(invitation) && invitation.getStatus() != InvitationStatus.DECLINED) {
            invitation.setStatus(InvitationStatus.DECLINED);
            invitationServices.save(invitation);
            return returnOK();
        }

        return rejectInvitationHandle();
    }

    private ResponseEntity<String> rejectInvitationHandle() {
        return new ResponseEntity<>("You have not permission to handle this invitation", HttpStatus.FORBIDDEN);
    }

    private ResponseEntity<String> returnOK() {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "authByToken", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public @ResponseBody
    User authByToken(String token) {
        UserAuthenticator userAuthenticator = new GoogleUserAuthenticator();
        User authUser = userAuthenticator.authenticateUser(token);

        userServices.save(authUser);

        Authentication auth = new UsernamePasswordAuthenticationToken(authUser, token, authorities);
        SecurityContextHolder.getContext().setAuthentication(auth);

        return authUser;
    }

    @GetMapping(value = "sharedWithMe")
    public List<User> getSharedWithMe() {
        return userServices.getSharedWithMe(getCurrentUser());
    }

    @PostMapping({"addFCMToken"})
    public void addFCMToken(String token) {
        try {
            TopicManagementResponse response = fcmServices.addTokenToTopic(getCurrentUser().getUserId(), token);
            for (TopicManagementResponse.Error error : response.getErrors()) {
                System.out.println(this.getClass().getName() + "#addFCMToken: error " + error);
            }
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
    }

    private User getCurrentUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}

