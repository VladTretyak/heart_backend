package co.neatapps.heart.heartbackend.controllers;

import co.neatapps.heart.heartbackend.domain.Comment;
import co.neatapps.heart.heartbackend.services.CommentServices;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("comments")
public class CommentController {
    private CommentServices commentServices;

    public CommentController(CommentServices commentServices) {
        this.commentServices = commentServices;
    }

    @GetMapping("{id}")
    public Comment getCommentById(@PathVariable Long id) {
        return this.commentServices.findById(id);
    }

    @PostMapping
    public Comment saveComment(@RequestBody Comment comment) {
        return this.commentServices.save(comment);
    }

    @DeleteMapping("{id}")
    public void deleteComment(@PathVariable Long id) {
        this.commentServices.delete(id);
    }
}
