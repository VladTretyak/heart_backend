package co.neatapps.heart.heartbackend.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
public class User {
    @Id
    private String userId;
    private String name;
    private String photo;
    private String email;
    private String familyName;
    private String givenName;
    @ElementCollection(targetClass = User.class, fetch = FetchType.EAGER)
    @JsonIgnore
    private List<User> sharedTo;
    @Transient
    private Collection<Storm> storms;

    public User() {
    }

    public User(String userId, String name, String photo, String email, String familyName, String givenName) {
        this.userId = userId;
        this.name = name;
        this.photo = photo;
        this.email = email;
        this.familyName = familyName;
        this.givenName = givenName;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFamilyName() {
        return this.familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getGivenName() {
        return this.givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public List<User> getSharedTo() {
        return sharedTo;
    }

    public void setSharedTo(List<User> sharedTo) {
        this.sharedTo = sharedTo;
    }

    public Collection<Storm> getStorms() {
        return this.storms;
    }

    public void setStorms(Collection<Storm> storms) {
        this.storms = storms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!userId.equals(user.userId)) return false;
        if (!name.equals(user.name)) return false;
        return email.equals(user.email);
    }

    @Override
    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }
}
