package co.neatapps.heart.heartbackend.domain;

public enum InvitationStatus {
    PENDING,
    ACCEPTED,
    DECLINED
}
