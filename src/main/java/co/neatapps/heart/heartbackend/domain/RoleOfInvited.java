package co.neatapps.heart.heartbackend.domain;

public enum RoleOfInvited {
    COUNSELOR,
    WARD
}
