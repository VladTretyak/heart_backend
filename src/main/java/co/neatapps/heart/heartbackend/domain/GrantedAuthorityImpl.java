package co.neatapps.heart.heartbackend.domain;

import org.springframework.security.core.GrantedAuthority;

public class GrantedAuthorityImpl implements GrantedAuthority {
    private AuthorityRoles auth;

    public GrantedAuthorityImpl(AuthorityRoles auth) {
        this.auth = auth;
    }

    @Override
    public String getAuthority() {
        return auth.name();
    }
}