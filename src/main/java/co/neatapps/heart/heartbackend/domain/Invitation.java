package co.neatapps.heart.heartbackend.domain;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Invitation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "owner")
    private User owner;
    private String emailOfInvited;
    @Enumerated(EnumType.STRING)
    private RoleOfInvited roleOfInvited;
    private Date invitationDate;
    @Enumerated(EnumType.STRING)
    private InvitationStatus status;
    private String userIdOfInvited;
    private String nameOfInvited;
    private String photoOfInvited;

    public Invitation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getEmailOfInvited() {
        return emailOfInvited;
    }

    public void setEmailOfInvited(String emailOfInvited) {
        this.emailOfInvited = emailOfInvited;
    }

    public RoleOfInvited getRoleOfInvited() {
        return roleOfInvited;
    }

    public void setRoleOfInvited(RoleOfInvited roleOfInvited) {
        this.roleOfInvited = roleOfInvited;
    }

    public Date getInvitationDate() {
        return invitationDate;
    }

    public void setInvitationDate(Date invitationDate) {
        this.invitationDate = invitationDate;
    }

    public InvitationStatus getStatus() {
        return status;
    }

    public void setStatus(InvitationStatus status) {
        this.status = status;
    }

    public String getUserIdOfInvited() {
        return userIdOfInvited;
    }

    public void setUserIdOfInvited(String userIdOfInvited) {
        this.userIdOfInvited = userIdOfInvited;
    }

    public String getNameOfInvited() {
        return nameOfInvited;
    }

    public void setNameOfInvited(String nameOfInvited) {
        this.nameOfInvited = nameOfInvited;
    }

    public String getPhotoOfInvited() {
        return photoOfInvited;
    }

    public void setPhotoOfInvited(String photoOfInvited) {
        this.photoOfInvited = photoOfInvited;
    }
}
