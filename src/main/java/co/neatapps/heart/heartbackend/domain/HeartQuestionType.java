package co.neatapps.heart.heartbackend.domain;


public enum HeartQuestionType {
    HLetter,
    ELetter,
    ALetter,
    RLetter,
    TLetter;

    private HeartQuestionType() {
    }
}
