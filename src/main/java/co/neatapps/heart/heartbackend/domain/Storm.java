package co.neatapps.heart.heartbackend.domain;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Storm {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer stormId;
    private int intensity;
    private Date creationDate;
    private String emotion;
    private String userId;
    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = {CascadeType.ALL},
            orphanRemoval = true
    )
    @JoinColumn(name = "stormId")
    private List<HeartQuestion> heartQuestions;

    public Storm() {
    }



    public Integer getStormId() {
        return this.stormId;
    }

    public void setStormId(Integer stormId) {
        this.stormId = stormId;
    }

    public int getIntensity() {
        return this.intensity;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getEmotion() {
        return this.emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setHeartQuestions(List<HeartQuestion> heartQuestions) {
        this.heartQuestions = heartQuestions;
    }

    public List<HeartQuestion> getHeartQuestions() {
        return this.heartQuestions;
    }

}
