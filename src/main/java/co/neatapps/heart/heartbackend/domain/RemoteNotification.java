package co.neatapps.heart.heartbackend.domain;

import java.util.HashMap;
import java.util.Map;

public class RemoteNotification {
    private String title;
    private String body;
    private String channelId;
    private Map<String, String> data;

    private RemoteNotification(Builder builder) {
        this.title = builder.title;
        this.body = builder.body;
        this.channelId = builder.channelId;
        this.data = builder.data;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getChannelId() {
        return channelId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String get(String key) {
        return data.get(key);
    }

    public Map<String, String> getData() {
        return data;
    }

    /**
     * @return map with all fields
     */
    public Map<String, String> getAllData() {
        Map<String, String> data = new HashMap<>(this.data);
        data.put("title", this.title);
        data.put("body", this.body);
        data.put("channelId", this.channelId);
        return data;
    }

    public static class Builder {
        private String title;
        private String body;
        private String channelId;
        private Map<String, String> data = new HashMap<>();

        private Builder() {

        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setBody(String body) {
            this.body = body;
            return this;
        }

        public Builder setChannelId(String channelId) {
            this.channelId = channelId;
            return this;
        }

        public Builder putData(String key, String value) {
            this.data.put(key, value);
            return this;
        }

        public Builder setData(Map<String, String> data) {
            this.data = data;
            return this;
        }

        public RemoteNotification build() {
            return new RemoteNotification(this);
        }
    }
}
