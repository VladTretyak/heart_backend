package co.neatapps.heart.heartbackend.domain;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class HeartQuestion {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;
    private String description;
    private String status; //TODO change for Enum
    private HeartQuestionType heartQuestionType;
    private Date date;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "heartQuestion")
    private List<Comment> comments;


    public HeartQuestion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public HeartQuestionType getHeartQuestionType() {
        return heartQuestionType;
    }

    public void setHeartQuestionType(HeartQuestionType heartQuestionType) {
        this.heartQuestionType = heartQuestionType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HeartQuestion that = (HeartQuestion) o;

        if (!id.equals(that.id)) return false;
        if (heartQuestionType != that.heartQuestionType) return false;
        return date.equals(that.date);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + heartQuestionType.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }
}
