package co.neatapps.heart.heartbackend.domain;

public enum AuthorityRoles {
    USER,
    ADMIN
}
