package co.neatapps.heart.heartbackend.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class Requestor {

    private HttpURLConnection connection;

    public static class Builder {
        private String url;
        private String method = "POST";
        private boolean doInput = true;
        private boolean doOutput = true;
        private boolean useCaches = false;
        private Map<String, String> headers = new HashMap<>();
        Requestor requestor;

        public Builder(String url) {
            this.requestor = new Requestor();
            this.url = url;
        }

        public Builder setMethod(String method) {
            this.method = method;
            return this;
        }

        public Builder setDoInput(boolean doInput) {
            this.doInput = doInput;
            return this;
        }

        public Builder setDoOutput(boolean doOutput) {
            this.doOutput = doOutput;
            return this;
        }

        public Builder setHeader(String key, String value) {
            headers.put(key, value);
            return this;
        }

        public Builder setUseCaches(boolean useCaches) {
            this.useCaches = useCaches;
            return this;
        }

        public Requestor connect() throws Exception {
            URL url = new URL(this.url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setUseCaches(false);
            conn.setDoInput(this.doInput);
            conn.setDoOutput(this.doOutput);
            conn.setRequestMethod(this.method);

            for (Map.Entry<String, String> header : headers.entrySet()) {
                conn.setRequestProperty(header.getKey(), header.getValue());
            }
            this.requestor.setConnection(conn);
            return this.requestor;
        }
    }

    public void setConnection(HttpURLConnection connection) {
        this.connection = connection;
    }

    public void write(String body) throws Exception {
        OutputStreamWriter wr = new OutputStreamWriter(this.connection.getOutputStream());
        wr.write(body);
        wr.flush();
    }

    public String read() throws Exception {
        BufferedReader in = new BufferedReader(new InputStreamReader(this.connection.getInputStream(),
                Charset.forName("UTF-8")));
        String inputLine;
        StringBuilder stringBuilder = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            stringBuilder.append(inputLine);
        }
        in.close();
        return stringBuilder.toString();
    }
}
