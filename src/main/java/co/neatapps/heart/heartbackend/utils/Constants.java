package co.neatapps.heart.heartbackend.utils;

public class Constants {
    public final static String FCM_API_URL = "https://fcm.googleapis.com/v1/projects/heart-demo/messages:send";
    public final static String SECRET_FILE = "json/heart-demo-firebase-adminsdk-59c4l-6aab876fe0.json";
    public final static String DATABASE_URL = "https://heart-demo.firebaseio.com/";

    public static final String SERVICE_CHANNEL = "co.neatapps.heart.service_channel";
    public static final String STORM_CHANNEL = "co.neatapps.heart.storm_channel";
    public static final String INVITATION_CHANNEL = "co.neatapps.heart.invitation_channel";
    public static final String CHAT_CHANNEL = "co.neatapps.heart.chat_channel";
}
